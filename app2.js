import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";
import {
  getStorage,
  ref,
  uploadBytesResumable,
  getDownloadURL,
  listAll,
  deleteObject,
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-storage.js";

import {
  getAuth,
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  signOut,
  onAuthStateChanged,
  AuthErrorCodes,
  GoogleAuthProvider,
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";

const firebaseConfiguration = {
  apiKey: "AIzaSyBoHRxroPD0h9nF-XR56kPcsxiYyXyYJys",
  authDomain: "pojectnr1.firebaseapp.com",
  databaseURL:
    "https://pojectnr1-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "pojectnr1",
  storageBucket: "pojectnr1.appspot.com",
  messagingSenderId: "387895958760",
  appId: "1:387895958760:web:3b084c1d2d32f651313d5d",
  measurementId: "G-42K01R9C72",
};

const app = initializeApp(firebaseConfiguration);
const storage = getStorage();

const miniCartCount = document.querySelector("#miniCartCount");
const buyButton = document.querySelector("#buyButton");
const productsSection = document.querySelector("#productsSection");
const oneProductArea = document.querySelector("#oneProductArea");
const productPicture = document.querySelector("#productPicture");
const productDetails = document.querySelector("#productDetails");
const productDescription = document.querySelector("#productDescription");
const productPrice = document.querySelector("#productPrice");
const price = document.querySelector("#price");
const newProductName = document.querySelector("#newProductName");
const newProductPrice = document.querySelector("#newProductPrice");
const selectProductButton = document.querySelector("#selectProductButton");
const uploadProductButton = document.querySelector("#uploadProductButton");
const newProduct = document.querySelector("#newProduct");
const newAddedProduct = document.querySelector("#newAddedProduct");
const newproductStorage = document.querySelector("#newproductStorage");
const addNewProductButton = document.querySelector("#addNewProductButton");
const removeNewProductButton = document.querySelector("#removeNewProductButton");
const newpriceStorage = document.querySelector("#newpriceStorage");

// Zmienne pomocnicze do przechowywania oraz wrzucania plików
const fileReader = new FileReader(); //interfejs Windows wybierania pliku
let clothesFiles = [];
let clothesPrice = [];

// Wyświetli się jaki typ pliku wybrano:
const input = document.createElement("input");
input.type = "file";
// Wybieranie pliku
input.onchange = (event) => {
  clothesFiles = event.target.files;
  fileReader.readAsDataURL(clothesFiles[0]);

  //dzielenie nazwy pliku na dwie części
  const prNameIntoParts = clothesFiles[0].name.split(".");
  const prName = prNameIntoParts[0]; //lewa część nazwy (przed .)
  newProductName.value = prName; //wpisuje nazwę w newProductName
};

selectProductButton.onclick = () => {
  input.click();
};

// Uploadowanie pliku do Storage po kliknięciu przycisku UploadImageButton

uploadProductButton.onclick = async () => {
  const productToUpload = clothesFiles[0]; //przesyłamy plik
  const productFullName = newProductName.value; //przesyłamy  nazwę pliku i rozszerzenie
  
  const metaData = {
    contentType: productToUpload.type, //typ danych, jakie przesyłamy
  };
  //console.log(productToUpload, productFullName, metaData);
  const storage = getStorage(); //pobieramy funkcję getStorage
  const storageCatalog = ref(
    storage,
    "clothes/" + productFullName //+ newprPrice
  ); //wybieramy katalog, w którym będą zapisywane obrazki| Pobieramy funkcję ref
  const uploadProcess = uploadBytesResumable(
    storageCatalog,
    productToUpload,
    metaData
  );

  //dodawanie obiektu jako nowy div
  // const newProductDiv = document.createElement("div");
  // console.dir(document.getElementById("newAddedProduct").appendChild(newProductDiv));
};

//dodawanie produktów
const productRef = ref(storage, "clothes/spodenki");
//const newprPrice = newProductPrice.value;

addNewProductButton.onclick = async () => {
  getDownloadURL(productRef)
    .then((url) => {
      const xhr = new XMLHttpRequest();
      xhr.responseType = "blob";
      xhr.onload = (event) => {
        const blob = xhr.response;
      };
      xhr.open("GET", url);
      xhr.send();
      newproductStorage.setAttribute("src", url);
      
    })
    .catch((error) => {
      console.warn(error);
    });
};

//usuwanie produktów
const desertRef = ref(storage, "clothes/spodenki");

removeNewProductButton.onclick = async () => {
  deleteObject(desertRef)
    .then(() => {})
    .catch((error) => {
      console.warn(error);
    });
};
