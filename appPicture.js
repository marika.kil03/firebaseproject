import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";
import {
  getStorage,
  ref,
  uploadBytesResumable,
  getDownloadURL,
  listAll,
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-storage.js";

const firebaseConfiguration = {
  apiKey: "AIzaSyBoHRxroPD0h9nF-XR56kPcsxiYyXyYJys",
  authDomain: "pojectnr1.firebaseapp.com",
  databaseURL:
    "https://pojectnr1-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "pojectnr1",
  storageBucket: "pojectnr1.appspot.com",
  messagingSenderId: "387895958760",
  appId: "1:387895958760:web:3b084c1d2d32f651313d5d",
  measurementId: "G-42K01R9C72",
};

// Inicjalizacja aplikacji i Storage
const app = initializeApp(firebaseConfiguration);
const storage = getStorage();
//definicja zmiennych
const imageName = document.querySelector("#imageName");
const imageExtension = document.querySelector("#imageExtension");
const SelectImageButton = document.querySelector("#SelectImageButton");
const UploadImageButton = document.querySelector("#UploadImageButton");
const ProcessStatusBar = document.querySelector("#ProcessStatusBar");
const imageBox = document.querySelector("#imageBox");

// Zmienne pomocnicze do przechowywania oraz wrzucania plików
const fileReader = new FileReader(); //interfejs Windows wybierania pliku
let files = [];
// Wyświetli się jaki typ pliku wybrano:
const input = document.createElement("input");
input.type = "file";
// Wybieranie pliku
input.onchange = (event) => {
  files = event.target.files;
  fileReader.readAsDataURL(files[0]);
  //dzielenie nazwy pliku na dwie części
  const imgNameIntoParts = files[0].name.split(".");
  const imgName = imgNameIntoParts[0]; //lewa część nazwy (przed .)
  const imgExtension = "." + imgNameIntoParts[1]; //prawa część nazwy (po .)
  imageName.value = imgName;
  imageExtension.innerHTML = imgExtension;
};

SelectImageButton.onclick = () => {
  input.click();
};

// Uploadowanie pliku do Storage po kliknięciu przycisku UploadImageButton
UploadImageButton.onclick = async () => {
  const imageToUpload = files[0]; //przesyłamy plik
  const imageFullName = imageName.value + imageExtension.innerHTML; //przesyłamy  nazwę pliku i rozszerzenie
  const metaData = {
    contentType: imageToUpload.type, //typ danych, jakie przesyłamy
  };
  //console.log(imageToUpload, imageFullName, metaData);

  const storage = getStorage(); //pobieramy funkcję getStorage
  const storageCatalog = ref(storage, 'Pictures/' + imageFullName); //wybieramy katalog, w którym będą zapisywane obrazki| Pobieramy funkcję ref
  const uploadProcess = uploadBytesResumable(
    storageCatalog,
    imageToUpload,
    metaData
  ); //pasek postępu
  uploadProcess.on(
    "state-changed",
    (snapshot) => {
      const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      ProcessStatusBar.innerHTML = progress + "% załadowano";
    },
    (error) => {
      console.warn(error);
    },
    () => {
      getDownloadURL(uploadProcess.snapshot.ref).then((url) => {
        imageBox.setAttribute("src", url);
      });
    }
  );
};

const getFilesButton = document.querySelector("#getFilesButton");
const allFilesList = document.querySelector("#allFilesList");
const getImageButton = document.querySelector("#getImageButton");
const imageStorage = document.querySelector("#imageStorage");

// Pobranie listy wszystkich dostępnych plików
const imageFolderRef = ref(storage, 'Pictures'); //podajemy folder, w jakim przechowywane są obrazki
console.log(imageFolderRef);
getFilesButton.onclick = async () => {
  listAll(imageFolderRef)
    .then((list) => {
      list.items.forEach((fileRef) => {  //z listy wyciągane są wszystkie elementy
        allFilesList.innerHTML = fileRef;
      });
    })
    .catch((error) => {
      console.warn(error);
    });
};


// Pobranie konkretnego obrazka
const imageRef = ref(storage, 'Pictures/molly.jpg');

getImageButton.onclick = async () => {
getDownloadURL(imageRef)
.then((url) => {
const xhr = new XMLHttpRequest();
xhr.responseType = 'blob';
xhr.onload = (event) => {
const blob = xhr.response;
};
xhr.open('GET', url);
xhr.send();
imageStorage.setAttribute('src', url);
})
.catch((error) => {
console.warn(error);
});
}

