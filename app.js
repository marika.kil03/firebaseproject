import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";
import {
  getAuth,
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  signOut,
  onAuthStateChanged,
  AuthErrorCodes,
  GoogleAuthProvider,
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";

const firebaseConfiguration = {
  apiKey: "AIzaSyBoHRxroPD0h9nF-XR56kPcsxiYyXyYJys",
  authDomain: "pojectnr1.firebaseapp.com",
  databaseURL:
    "https://pojectnr1-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "pojectnr1",
  storageBucket: "pojectnr1.appspot.com",
  messagingSenderId: "387895958760",
  appId: "1:387895958760:web:3b084c1d2d32f651313d5d",
  measurementId: "G-42K01R9C72",
};

// Inicjalizacja aplikacji i Auth
const app = initializeApp(firebaseConfiguration);

const auth = getAuth(app);

//Połączenie z elementami z HTML
const FormEmail = document.querySelector("#FormEmail");
const FormPassword = document.querySelector("#FormPassword");
const LogInButton = document.querySelector("#LogInButton");
const SignUpButton = document.querySelector("#SignUpButton");
const ErrorMessage = document.querySelector("#ErrorMessage");
const LogOutButton = document.querySelector("#LogOutButton");

//Logowanie w aplikacji
const userLogin = async () => {
  const emailValue = FormEmail.value;
  const passwordValue = FormPassword.value;

  try {
    const user = await signInWithEmailAndPassword(
      auth,
      emailValue,
      passwordValue
    );
    //console.log(user);
  } catch (error) {
    if (error.code == AuthErrorCodes.INVALID_PASSWORD) {
      ErrorMessage.innerHTML = "Wpisano złe hasło";
    } else {
      ErrorMessage.innerHTML = "Podany użytkownik nie istnieje";
    }
  }
};

//Podłączenie przycisku LogInButton
LogInButton.addEventListener("click", userLogin);

//Rejestracja użytkownika

const userSignIn = async () => {
  const emailValue = FormEmail.value;
  const passwordValue = FormPassword.value;
  try {
    const user = await createUserWithEmailAndPassword(
      auth,
      emailValue,
      passwordValue
    );
  } catch (error) {
    console.warn(error);
  }
};

//Podłączenie przycisku SignUpButton
SignUpButton.addEventListener("click", userSignIn);

//Wylogowanie użytkownika
const userLogOut = async () => {
  await signOut(auth);
  console.log("Wylogowano");
};
//Podłączenie przycisku LogOutButton
LogOutButton.addEventListener("click", userLogOut);

// Zmiana panelu logowania
const authStateChanged = async () => {
  onAuthStateChanged(auth, (user) => {
    if (user) {
      LoginForm.style.display = "none";
      LogOutSection.style.display = "block";
    } else {
      LoginForm.style.display = "block";
      LogOutSection.style.display = "none";
    }
  });
};
authStateChanged();

//  11*. Pokaż zalogowanym użytkownikom zawartość kolekcji stworzonej w bazie danych.
//  12*. Dodanie możliwości usuwanie dokumentu z poziomu UI zalogowanym użytkownikom
//  13*. Dodanie możliwości edycji dokumentu z poziomu UI zalogowanym użytkownikom
//  14*. Dodanie możliwości logowanie poprzez konto Google
const provider = new GoogleAuthProvider();
